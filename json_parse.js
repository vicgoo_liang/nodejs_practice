var json = JSON.parse('{"1": 1, "2": 2, "3": {"4": 4, "5": {"6": 6}}}', function(k, v) {
  if(v==6){
      return k+"_"+v;       // 返回修改的值 会修改对应看的value
  }
  console.log("----------k:-----------"); // 输出当前属性，最后一个为 ""
  console.log(k); // 输出当前属性，最后一个为 ""
  console.log("----------v:-----------"); // 输出当前属性，最后一个为 ""
  console.log(v); // 输出当前属性，最后一个为 ""
  return v;       // 返回修改的值
});

console.log("----------json:-----------"); // 输出当前属性，最后一个为 ""
console.log(json); // 输出当前属性，最后一个为 ""
