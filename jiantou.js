//具有一个参数的简单函数
var single = a => a;

//var single = function(a){
//return a
//}
console.log(single('hello, world')) // 'hello, world'

//没有参数的需要用在箭头前加上小括号
var log = () => {
    console.log('no param')
}
//var log =function(){
//   console.log('no param')
//}
log();
//多个参数需要用到小括号，参数间逗号间隔，例如两个数字相加
var add = (a, b) => a + b

//var add = funciton(a, b){
//return a + b;
//}
console.log(add(3, 8)) // 11
//函数体多条语句需要用到大括号
var add2 = (a, b) => {
    if (typeof a == 'number' && typeof b == 'number') {
        return a + b
    } else {
        return 0
    }
}

//var add2  = function(a, b) {
//    if (typeof a == 'number' && typeof b == 'number') {
//        return a + b
//   } else {
//        return 0
//    }
//}
console.log(add(3, 8)) // 11
// 返回对象时需要用小括号包起来，因为大括号被占用解释为代码块了

var getHash = arr => {
    // ...
    console.log(arr);
    return ({
        name: 'Jack',
        age: 33
    })
}
//var getHash = function(arr){
//    return ({
//        name: 'Jack',
//        age: 33
//    })
//}

console.log(getHash([1,2]));//{ name: 'Jack', age: 33 }

 //直接作为事件handler

//document.addEventListener('click', ev => {
//    console.log(ev)
//})


//作为数组排序回调
var arr = [1, 9 , 2, 4, 3, 8].sort((a, b) => {
    if (a - b > 0 ) {
        return 1
    } else {
        return -1
    }
})
// var arr = = [1, 9 , 2, 4, 3, 8].sort(function(a,b){
 // if (a - b > 0 ) {
//        return 1
//    } else {
 //       return -1
 //   }
//
// });
console.log(arr);
//arr // [1, 2, 3, 4, 8, 9]
