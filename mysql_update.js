var mysql  = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  port: '3306',
  database: 'test',
  charset:'UTF8_GENERAL_CI'
});

connection.connect();

var modSql = 'UPDATE tp_name SET name = ? WHERE Id = ?';
var modSqlParams = ['菜鸟移动站',1];
//改
connection.query(modSql,modSqlParams,function (err, result) {
   if(err){
         console.log('[UPDATE ERROR] - ',err.message);
         return;
   }
  console.log('--------------------------UPDATE----------------------------');
  console.log('UPDATE affectedRows',result.affectedRows);
  console.log('-----------------------------------------------------------------\n\n');
});

connection.end();
