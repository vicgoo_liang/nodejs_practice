//event.js 文件
var events = require('events');
var emitter = new events.EventEmitter();
var handler = function handler(arg1, arg2) {
    console.log('on listener1', arg1, arg2);
}
emitter.on('someEvent', handler);

//emitter.emit('someEvent', 'arg1 参数', 'arg2 参数');
emitter.removeListener('someEvent', handler);


emitter.emit('someEvent', 'arg1 参数', 'arg2 参数');

