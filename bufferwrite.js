buf = Buffer.alloc(256);
console.log(buf);
len = buf.write("www.runoob.com");
console.log(buf);
console.log("写入字节数 : "+  len);

buf2 = Buffer.alloc(26);
console.log(buf2);
console.log(buf);
for (var i = 0 ; i < 26 ; i++) {
  buf2[i] = i + 97;
}

console.log( buf2.toString('ascii'));       // 输出: abcdefghijklmnopqrstuvwxyz
console.log( buf2.toString('ascii',0,5));   // 输出: abcde
console.log( buf2.toString('utf8',0,5));    // 输出: abcde
console.log( buf2.toString(undefined,0,5)); // 使用 'utf8' 编码, 并输出: abcde

