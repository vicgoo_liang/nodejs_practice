var buffer1 = Buffer.from(('李小妍'));
console.log(buffer1); // 输出当前属性，最后一个为 ""
console.log("buffer length: " + buffer1.length);
var buffer2 = Buffer.from(('www.runoob.com'));
console.log("buffer length: " + buffer2.length);
console.log(buffer2); // 输出当前属性，最后一个为 ""
var buffer3 = Buffer.concat([buffer1,buffer2]);
console.log(buffer3); // 输出当前属性，最后一个为 ""
console.log("buffer3 内容: " + buffer3.toString());
console.log("buffer length: " + buffer3.length);

var buffer4 = Buffer.from([0xe8,0x8f,0x9c]);
console.log(buffer4); // 输出当前属性，最后一个为 ""
console.log("buffer4 内容: " + buffer4.toString());
console.log("buffer length: " + buffer4.length);


var buffer5 = Buffer.from("runoob李小妍www");
console.log(buffer5); // 输出当前属性，最后一个为 ""
console.log("buffer4 内容: " + buffer5.toString());
console.log("buffer length: " + buffer5.length);
