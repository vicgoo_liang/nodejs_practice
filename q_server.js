// var http = require("http");
// var url = require("url");

// function start() {
//   function onRequest(request, response) {
//     var pathname = url.parse(request.url).pathname;
//     console.log("Request for " + pathname + " received.");
//     response.writeHead(200, {"Content-Type": "text/plain"});
//     response.write("Hello World");
//     response.end();
//   }

//   http.createServer(onRequest).listen(8880);
//   console.log("Server has started.");
// }

// exports.start = start;
var http = require("http");
var url = require("url");
var querystring = require("querystring");
function start(route) {
  function onRequest(request, response) {

    console.log("----------------request start--------------");
    //获取返回的url对象的query属性值
    var arg = url.parse(request.url).query;

    //将arg参数字符串反序列化为一个对象
    var params = querystring.parse(arg);
    //获取参数param
    console.log("----------------params--------------");
    console.log(params);
      //请求的方式
    console.log("----------------method--------------");
    console.log("method - " + request.method);
      //请求的url
    console.log("----------------url--------------");
    console.log("url - " + request.url);

    var pathname = url.parse(request.url).pathname;
     console.log("----------------pathname--------------");
    console.log("Request for " + pathname + " received.");

    route(pathname);

    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Hello World");
    response.end();
  }

  http.createServer(onRequest).listen(8880);
  console.log("Server has started.");
}

exports.start = start;
