var http = require('http');
var url = require('url');
var util = require('util');

http.createServer(function(req, res){
    res.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
    res.end(util.inspect(url.parse(req.url, true)));
}).listen(3000);
//http://127.0.0.1:3000/index?id=111111&ooo=123123

// Url {
//   protocol: null,
//   slashes: null,
//   auth: null,
//   host: null,
//   port: null,
//   hostname: null,
//   hash: null,
//   search: '?id=111111&ooo=123123',
//   query: { id: '111111', ooo: '123123' },
//   pathname: '/index',
//   path: '/index?id=111111&ooo=123123',
//   href: '/index?id=111111&ooo=123123' }
